section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, 60          ; 'exit' syscall number
    xor     rdi, rdi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rcx - счетчик
; нуль-термированная - это оканчивающаяся на 0x00
string_length:
    xor rcx, rcx
    mov rax, rdi
    .loop:
        mov rax, [rdi + rcx]
        inc rcx
        test al, 0xFF
        jnz .loop
    dec rcx
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - input string

; rax - системный вызов
; rdi - stdout
; rsi - строка
; rdx - длинна
print_string:
    call string_length ; длинна
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
; rdi - символ печати
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rsi, 0xA
    mov rdx, 1
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - число
print_uint:
    mov rax, rdi
    mov r8, 0x0A
    push 0x00
    .loop:
        xor rdx, rdx
        div r8          ; rax - частное, rdx - остаток
        add rdx, 0x30   ; '0' в ASCII таблице
        push rdx
        cmp rax, r8
        jae .loop
    add rax, 0x30
    cmp rax, 0x30
    je .next
    push rax

    .next:
        pop rdi
        cmp rdi, 0x00
        je .eof
        call print_char
        jmp .next
    .eof:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - число
print_int:
    mov r8, rdi
    test rdi, rdi
    jns .print
    mov rdi, 0x2d
    call print_char
    mov rdi, r8
    neg rdi
    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - первый
; rsi - второй
string_equals:
    xor rcx, rcx
    .loop:
        mov al, byte[rdi+rcx]
        mov dl, byte[rsi+rcx]
        cmp al,dl
        jne .neqls
        inc rcx
        cmp al, 0x00
        je .eqls
        jmp .loop
    .eqls:
        mov rax, 1
        ret
    .neqls:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; rdi - начало буфера
; rsi - размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера. 
; Эта функция должна дописывать к слову нуль-терминатор
section .data
    buf times 256 db 0

section .text
read_word:
    push rbx
	mov r8, rsi
	mov r9, rdi
    xor  rbx, rbx
    xor  rdi, rdi
    mov  rdx, 1
    .space:
        xor rax, rax
        mov rsi, buf
        syscall
        cmp al, 0 
        je  .finally 
        cmp byte[buf], 0x21 
        jb  .space 
        inc rbx 
    .read:
        xor rax, rax 
        lea rsi, [buf + rbx]
        syscall
        cmp byte [buf + rbx], 0x21
        jb  .finally
        cmp r8, rbx 
        jbe .exit
        inc rbx
        jmp .read
    .finally:
        mov byte[buf + rbx], 0 
        mov rdx, rbx
        mov rax, buf
        pop rbx
        ret
    .exit:
        xor rdx, r8
        xor rax, rax
        pop rbx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor r8, r8
    mov r10, 0x0A
    .num:
        cmp byte[rdi+rcx], 0x30
        jb .not_correct
        cmp byte[rdi+rcx], 0x39
        ja .not_correct
        mul r10
        mov r8b, byte[rdi+rcx]
        sub r8b, 0x30
        add rax, r8
        inc rcx
        jmp .num
    .not_correct:
        mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длинна буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rdx

    call string_length
    cmp rdx, rax
    jle .carry
    
    pop rdx
    pop rdi
    push rdi
    push rdx
    .loop:
        xor rcx, rcx
        mov cl, byte[rdi]
        mov byte[rsi], cl
        inc rdi
        inc rsi
        test rcx, rcx    
        jnz .loop
    pop rdx
    pop rdi
    ret
    .carry:
        pop rdx
        pop rdi
        mov rax, 0
        ret
